# Snake

![Some footage of the game](out.gif)

## Features

- A green snake that starts off ten blocks long
- Hitting the snake or a screen edge ends the game
- Eating red food elongates the snake
- Pressing a movement key (↑←↓→, WASD, KHJL) turns the snake *instantly*, unlike
  in the original game
- Holding a direction key makes the snake accelerate (unlike in the original
  game), which is useful for moving across large screens
- Very low memory usage (even a straight snake of infinite length only uses 8 B)
- Very low CPU usage
  - One thread sleeps and wakes up every 200ms to advance the snake
  - The other thread sleeps and wakes up every 16ms to check for input

## Building, installing and running

```sh
# 1. Extract the sources
tar xf snake-v0.9.0.tar.xf

# 2. Build the executable
cd snake-v0.9.0
./configure
make -j$(nproc)

# 3. Run without installing (optional)
bin/snake

# 4. Install and run the executable (installing requires root)
make install
hash -r
snake
```
