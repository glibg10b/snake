#ifndef LOWER_POINT_HPP
#define LOWER_POINT_HPP

#include "Direction.hpp"
#include "Point.hpp"

void move(Point<auto>& p, Direction direction) {
	switch (direction)
	{
		using enum Direction;
	case left:  --p.x(); break;
	case right: ++p.x(); break;
	case up:    --p.y(); break;
	case down:  ++p.y(); break;
	}
}

bool outOfRange(Point<auto> point, Rectangle<auto> range) {
	return !between(point.x(), range.left(), range.right())
	|| !between(point.y(), range.top(), range.bot());
}

#endif // !LOWER_POINT_HPP
