#ifndef THREADINPUTHANDLER_HPP
#define THREADINPUTHANDLER_HPP

#include "ThreadSignal.hpp"
#include "Window.hpp"

#include <algorithm>
#include <functional>
#include <vector>

class ThreadInputHandler: public ThreadSignal {
public:
	template <typename... Key>
	ThreadInputHandler(std::function<void()> pred, Key... keys)
		: ThreadSignal{ pred }, m_keys{ keys... } // keys to register
	{}

	// Trigger if key is registered
	void triggerIfRegistered(ThreadSignalWaiter& waiter, char_t key) {
		if (std::ranges::any_of(m_keys,
					[key](auto c){ return c == key; }
					)) trigger(waiter);
	}
private:
	std::vector<char_t> m_keys{};
};

#endif // !THREADINPUTHANDLER_HPP
