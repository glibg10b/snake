#include "Direction.hpp"
#include "Point.hpp"
#include "Screen.hpp"
#include "Snake.hpp"
#include "ThreadInputHandler.hpp"
#include "ThreadSignalWaiter.hpp"
#include "Window.hpp"
#include "keys.hpp"
#include "random.hpp"
#include "snake.hpp"

#include <array>
#include <atomic>
#include <future>
#include <iostream>
#include <random>
#include <thread>

using namespace std::literals;

template <std::size_t N>
using InputHandlers = std::array<ThreadInputHandler, N>;

template <typename URBG, std::size_t N>
void gameLoop(Snake<auto>& snake, Food& food, const Window& window, URBG& g,
	bool& resetTimer, Direction& direction, std::promise<void>& quitPromise,
	std::shared_future<void> quitFuture, ThreadSignalWaiter& waiter,
	InputHandlers<N>& inputHandlers, char_t snakeBody, char_t foodChar
) {
	constexpr auto tickLength{ 200ms };
	auto nextTick{ std::chrono::steady_clock::now() };
	while (true)
	{
		if (quitFuture.wait_for(0s) == std::future_status::ready)
			return;

		if (std::exchange(resetTimer, true))
			nextTick = std::chrono::steady_clock::now();

		nextTick += tickLength;

		if (waiter.waitForSignalUntil(nextTick))
		{
			for (auto& h: inputHandlers) h.handle();
			continue;
		}

		advance(snake, direction, food, window, g, quitPromise,
				snakeBody, foodChar);
	}
}

template <std::size_t N>
void inputLoop(const Window& window, InputHandlers<N>& inputHandlers,
	ThreadSignalWaiter& waiter, std::shared_future<void> quitFuture)
{
	while (true)
	{
		const auto ch{ window.get() };

		for (auto& h: inputHandlers)
			h.triggerIfRegistered(waiter, ch);

		if (quitFuture.wait_for(16ms) == std::future_status::ready)
			return;
	}
}

void initScreen(const Screen& screen) {
	screen.noLineBuffer();
	screen.noEcho();
	screen.noFlushOnInt();
	screen.hideCursor();
	screen.startColor();
}

void initWindow(const Window& window) {
	window.keypad();
	window.noDelay();
	window.useDefaultColors();
}

int main() {
	auto mt{ seededMt() };

	const Screen screen{};
	initScreen(screen);
	const Window window{};
	initWindow(window);

	Snake<Window::point_t> snake{};

	const auto snakeBody{ acs::block() | Attr{Color::green}
		| Attr{Attr::bold} };
	draw(snakeBody, snake, window);

	const auto foodChar{ acs::diamond() | Attr{Color::red}
		| Attr{Attr::bold} };
	Food food{ newFood(snake, mt, window, foodChar) };

	Direction direction{ Direction::right };
	bool resetTimer{ false };

	std::promise<void> quitPromise{};
	auto quitFuture{ quitPromise.get_future().share() };

	const auto turnSnake{
		[&](Direction d){
			turn(snake, d, direction, food, resetTimer, quitPromise,
					window, mt, snakeBody, foodChar);
		},
	};

	InputHandlers<5> signals{{
		{ [&]{ turnSnake(Direction::up);    }, key::up,    'k', 'w' },
		{ [&]{ turnSnake(Direction::down);  }, key::down,  'j', 's' },
		{ [&]{ turnSnake(Direction::left);  }, key::left,  'h', 'a' },
		{ [&]{ turnSnake(Direction::right); }, key::right, 'l', 'd' },
		{ [&]{ quit(quitPromise);           }, 'q'                  }
	}};
	ThreadSignalWaiter waiter{};

	std::jthread inputThread(inputLoop<signals.size()>, std::ref(window),
			std::ref(signals), std::ref(waiter), quitFuture);

	gameLoop(snake, food, window, mt, resetTimer, direction, quitPromise,
			quitFuture, waiter, signals, snakeBody, foodChar);
}
