#include "Attr.hpp"

#include <ncurses.h>

#include <cassert>

short Attr::nextColorPair{ 1 };

Attr::Attr(Color f, Color b) {
	m_attr = COLOR_PAIR(nextColorPair);
	init_pair(nextColorPair++, f, b);
}

Attr::Attr(Enum e) {
	switch (e)
	{
	case bold: m_attr = A_BOLD; break;
	default: assert(0);
	}
}
