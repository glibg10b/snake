#ifndef THREADSIGNAL_HPP
#define THREADSIGNAL_HPP

#include "ThreadSignalWaiter.hpp"

#include <functional>
#include <atomic>

// A thread-safe signal that can be triggered and handled
class ThreadSignal {
public:
	ThreadSignal(std::function<void()> pred)
		: m_pred{ pred } // Signal handler
	{}

	// Notify the ThreadSignalWaiter object and allow handle()ing
	void trigger(ThreadSignalWaiter& waiter) {
		m_signalled.store(true, std::memory_order_relaxed);
		waiter.notify();
	}

	// Handle the signal if allowed and disallow handle()ing
	void handle() {
		if(fetchClear(m_signalled, std::memory_order_relaxed)) m_pred();
	}
private:
	std::atomic<bool> m_signalled{ false };
	const std::function<void()> m_pred{};
};

#endif // !THREADSIGNAL_HPP
