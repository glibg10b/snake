#ifndef LINE_HPP
#define LINE_HPP

#include "Direction.hpp"
#include "Point.hpp"

// Direction of vertical line from a to b
Direction lineDirection(Point<auto> a, Point<auto> b) {
	if (a.x() == b.x())
	{
		if (a.y() > b.y()) return Direction::down;
		if (a.y() < b.y()) return Direction::up;
	}
	else if (a.y() == b.y())
	{
		if (a.x() > b.x()) return Direction::right;
		if (a.x() < b.x()) return Direction::left;
	}
	else assert(!"Diagonal line");

	assert(!"Overlapping points");
}

#endif // !LINE_HPP
