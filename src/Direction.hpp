#ifndef DIRECTION_HPP
#define DIRECTION_HPP

#include <cassert>

enum class Direction { left, right, up, down };

Direction oppositeOf(Direction direction) {
	switch (direction)
	{
		using enum Direction;
	case left:  return right;
	case right: return left;
	case up:    return down;
	case down:  return up;
	}
	assert(0);
	return Direction::right;
}

#endif // !DIRECTION_HPP
