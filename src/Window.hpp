#ifndef SNAKE_WINDOW_HPP
#define SNAKE_WINDOW_HPP

#include "Point.hpp"
#include "Rectangle.hpp"

#include <cstdint>
#include <mutex>

using char_t = std::int_fast32_t;

constexpr char_t noChar{ -1 };

// TODO: Don't use stdscr
class Window {
public:
	using size_t = int;
	using point_t = Point<Window::size_t>;
	using rect_t = Rectangle<Window::size_t>;
	void keypad() const;
	void noDelay() const;
	void useDefaultColors() const;

	void put(char_t ch, point_t p) const { put(ch, p.x(), p.y()); }
	rect_t dimensions() const;
	char_t get() const;
	void refresh() const;
private:
	void put(char_t, Window::size_t x, Window::size_t y) const;
	mutable std::mutex m_mutex{};
};

#endif // !SNAKE_WINDOW_HPP
