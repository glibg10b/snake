#include <chrono>
#include <random>

auto seededMt() {
	return std::mt19937_64{
		static_cast<std::mt19937_64::result_type>(
			std::chrono::high_resolution_clock::now()
			.time_since_epoch()
			.count()
			^ std::random_device{}()
		)
	};
}
