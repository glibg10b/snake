#ifndef ACS_HPP
#define ACS_HPP

#include "Window.hpp"

namespace acs {
	char_t block();
	char_t diamond();
}

#endif // !ACS_HPP
