#ifndef SNAKE_POINT_HPP
#define SNAKE_POINT_HPP

#include <algorithm>
#include <cassert>

inline bool between(auto a, auto b, auto c) {
	const auto [min, max]{ std::minmax(b, c) };
	return a >= min && a <= max;
}

template <typename T>
class Point;

inline bool horizontal(Point<auto> a, Point<auto> b);
inline bool vertical(Point<auto> a, Point<auto> b);

template <typename T>
class Point {
public:
	Point() = default;

	Point(const T& x, const T& y): m_x{ x }, m_y{ y }
	{}

	Point(T&& x, T&& y): m_x{ x }, m_y{ y }
	{}

	T x() const { return m_x; }
	T y() const { return m_y; }

	T& x() { return m_x; }
	T& y() { return m_y; }

	// a and b must form a horizontal or vertical line
	bool isBetween(auto a, auto b) {
		if (horizontal(a, b))
			return horizontal(*this, a)
				&& between(x(), a.x(), b.x());
		else if (vertical(a, b))
			return vertical(*this, a) && between(y(), a.y(), b.y());

		assert(!"Line isn't horizontal or vertical");
	}
private:
	T m_x{};
	T m_y{};
};

bool operator==(Point<auto> a, Point<auto> b) {
	return a.x() == b.x() && a.y() == b.y();
}

inline bool horizontal(Point<auto> a, Point<auto> b) { return a.y() == b.y(); }
inline bool vertical(Point<auto> a, Point<auto> b) { return a.x() == b.x(); }

#endif // !SNAKE_POINT_HPP
