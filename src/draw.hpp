#ifndef DRAW_HPP
#define DRAW_HPP

#include "Window.hpp"
#include "Point.hpp"
#include "line.hpp"

// Draw line from joint to joint+1
void drawLine(char_t c, Point<auto> p1, Point<auto> p2,
		const Window& window) {
	if (horizontal(p1, p2))
	{
		const auto [min,max]{ std::minmax(p1.x(), p2.x()) };
		for (auto x{ min }; x <= max; ++x)
			window.put(c, { x, p1.y() });

		return;
	}
	if (vertical(p1, p2))
	{
		const auto [min,max]{ std::minmax(p1.y(), p2.y()) };
		for (auto y{ min }; y <= max; ++y)
			window.put(c, { p1.x(), y });

		return;
	}

	assert("Line not horizontal or vertical");
}

#endif // !DRAW_HPP
