#ifndef THREADSIGNALWAITER_HPP
#define THREADSIGNALWAITER_HPP

#include <atomic>
#include <condition_variable>

// Return flag and clear it
inline bool fetchClear(std::atomic<bool>& flag, std::memory_order order) {
	bool expected{ true };
	return flag.compare_exchange_strong(
		expected,
		false,
		order
	);
}

// Can be notified and a notification can be waited for. Thread-safe.
class ThreadSignalWaiter {
public:
	ThreadSignalWaiter() = default;
	ThreadSignalWaiter(const ThreadSignalWaiter&) = delete;
	ThreadSignalWaiter& operator=(const ThreadSignalWaiter&) = delete;

	// Notify all threads
	void notify() {
		m_unclaimed.store(true, std::memory_order_release);
		m_cv.notify_one();
	}

	// Wait for a notification or until until, whichever comes first
	bool waitForSignalUntil(std::chrono::time_point<auto> until) {
		std::unique_lock lock{ m_mutex };
		while (true)
		{
			if (fetchClear(m_unclaimed, std::memory_order_acquire))
				return true;
			if (m_cv.wait_until(lock, until)
				== std::cv_status::timeout)
				return false;
		};
	}
private:
	std::condition_variable m_cv{};
	std::atomic<bool> m_unclaimed{ false };
	mutable std::mutex m_mutex{};
};

#endif // !THREADSIGNALWAITER_HPP
