#ifndef LOWERCASE_SNAKE_HPP
#define LOWERCASE_SNAKE_HPP

#include "Attr.hpp"
#include "Color.hpp"
#include "Snake.hpp"
#include "Window.hpp"
#include "draw.hpp"
#include "Direction.hpp"
#include "acs.hpp"
#include "point.hpp"

#include <future>
#include <random>

void draw(char_t c, const Snake<auto>& snake, const Window& window) {
	for (auto it{ snake.begin()+1 }; it != snake.end(); ++it)
		drawLine(c, it[-1], *it, window);

	window.refresh();
}

bool headWillCollide(const Snake<auto>& snake, auto head) {
	if (snake.joints() < 4 || head == snake.tail()) return false;
	//                    ^ minimum amt of joints needed to collide

	return snake.collidesWith(head);
}

bool cantAdvance(
	const Snake<auto>& snake,
	Direction direction,
	Window::rect_t dimensions
) {
	auto newHead{ snake.head() };
	move(newHead, direction);

	return outOfRange(newHead, dimensions)
		|| headWillCollide(snake, newHead);
}

using Food = Window::point_t;

template <typename URBG>
Food newFood(const Snake<auto>& snake, URBG&& g, const Window& window,
		char_t foodChar)
{
	const auto dimensions{ window.dimensions() };
	std::uniform_int_distribution
		x{ dimensions.left(), dimensions.right() }
		, y{ dimensions.top(), dimensions.bot() }
	;

	Window::point_t food{};

	do { food = { x(g), y(g) }; } while (snake.collidesWith(food));

	window.put(foodChar, food);
	window.refresh();

	return food;
}

template <typename URBG>
bool tryAdvance(Snake<auto>& snake, Direction direction, Food& food,
	const Window& window, URBG&& g, char_t snakeBody, char_t foodChar)
{
	if (cantAdvance(snake, direction, window.dimensions()))
		[[unlikely]] return false;

	if (snake.tryEat(direction, food))
		food = newFood(snake, g, window, foodChar);
	else
	{
		window.put(' ', snake.tail());
		snake.advance(direction);
	}

	window.put(snakeBody, snake.head());
	window.refresh();

	return true;
}

void quit(std::promise<void>& quitPromise) { quitPromise.set_value(); }

template <typename URBG>
void advance(Snake<auto>& snake, Direction direction, Food& food,
		const Window& window, URBG& g, std::promise<void>& quitPromise,
		char_t snakeBody, char_t foodChar)
{
	if (!tryAdvance(snake, direction, food, window, g, snakeBody, foodChar))
		quit(quitPromise);
}

template <typename URBG>
void turn(Snake<auto>& snake, Direction newDir, Direction& direction,
	Food& food, bool& resetTimer, std::promise<void>& quitPromise,
	const Window& window, URBG& g, char_t snakeBody, char_t foodChar
)
{
	if (newDir == oppositeOf(direction)) return;

	resetTimer = true;
	direction = newDir;
	advance(snake, direction, food, window, g, quitPromise, snakeBody,
			foodChar);
}

#endif // !LOWERCASE_SNAKE_HPP
