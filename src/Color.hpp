#ifndef COLOR_HPP
#define COLOR_HPP

class Color {
public:
	enum Enum{ ddefault, green, red };

	Color(Enum e): m_color{ e }
	{}

	operator short() const;
private:
	Enum m_color{};
};

#endif // !COLOR_HPP
