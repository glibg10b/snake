#include "Point.hpp"
#include "Window.hpp"

#include <ncurses.h>

void Window::keypad() const {
	std::lock_guard lock{ m_mutex };
	::keypad(stdscr, true);
}
void Window::noDelay() const {
	std::lock_guard lock{ m_mutex };
	nodelay(stdscr, true);
}
void Window::put(char_t ch, Window::size_t x, Window::size_t y) const {
	std::lock_guard lock{ m_mutex };
	mvaddch(y, x, ch);
}
char_t Window::get() const { return getch(); }
Window::rect_t Window::dimensions() const {
	Window::point_t p{};

	m_mutex.lock();
	getmaxyx(stdscr, p.y(), p.x());
	m_mutex.unlock();

	return { {0,0}, { p.x()-1, p.y()-1 } };
}
void Window::refresh() const {
	std::lock_guard lock{ m_mutex };
	::refresh();
}

void Window::useDefaultColors() const { use_default_colors(); }
