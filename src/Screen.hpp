#ifndef SNAKE_SCREEN_HPP
#define SNAKE_SCREEN_HPP

// RAII helper
class Screen {
public:
	Screen();
	~Screen();

	// Don't print characters as they're being typed
	void noEcho() const;

	// Make characters available immediately after typing them
	void noLineBuffer() const;

	// Flush screen on interrupt
	void noFlushOnInt() const;

	void hideCursor() const;

	// Must be called if using colors is desired
	void startColor() const;
};

#endif // !SNAKE_SCREEN_HPP
