#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include "Point.hpp"

template <typename T>
class Rectangle {
public:
	Rectangle(Point<T> topLeft, Point<T> botRight)
		: m_topLeft{ topLeft }, m_botRight{ botRight }
	{}

	auto top  () const { return m_topLeft .y(); }
	auto left () const { return m_topLeft .x(); }
	auto bot  () const { return m_botRight.y(); }
	auto right() const { return m_botRight.x(); }
private:
	Point<T> m_topLeft{};
	Point<T> m_botRight{};
};

#endif // !RECTANGLE_HPP
