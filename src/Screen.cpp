#include "Screen.hpp"

#include <ncurses.h>

Screen::Screen() {
	initscr();
}
Screen::~Screen() { endwin(); }
void Screen::noEcho() const { noecho(); }
void Screen::noLineBuffer() const { cbreak(); }
void Screen::noFlushOnInt() const { intrflush(stdscr, false); }
void Screen::hideCursor() const { curs_set(0); }
void Screen::startColor() const { start_color(); }
