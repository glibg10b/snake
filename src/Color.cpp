#include "Color.hpp"

#include <ncurses.h>

#include <cassert>

Color::operator short() const {
	switch (m_color)
	{
	case ddefault: return -1;
	case green:    return COLOR_GREEN;
	case red:      return COLOR_RED;
	default:
		assert(0); return -1;
	}
}
