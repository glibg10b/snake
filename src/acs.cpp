#include "Window.hpp"

#include <curses.h>

namespace acs {
	char_t block() { return ACS_BLOCK; }
	char_t diamond() { return ACS_DIAMOND; }
}
