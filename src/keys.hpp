#ifndef SNAKE_KEYS_HPP
#define SNAKE_KEYS_HPP

#include <cstdint>
namespace key {
	constexpr std::int_fast16_t down{ 0402 };
	constexpr std::int_fast16_t up{ 0403 };
	constexpr std::int_fast16_t left{ 0404 };
	constexpr std::int_fast16_t right{ 0405 };
}

#endif // !SNAKE_KEYS_HPP
