#ifndef SNAKE_HPP
#define SNAKE_HPP

#include "Direction.hpp"
#include "Point.hpp"
#include "line.hpp"

#include <atomic>
#include <deque>
#include <mutex>

template <typename T>
class Snake {
public:
	// Iterator to first joint
	auto begin() const { return m_joints.begin(); }

	// Iterator to last joint
	auto end() const { return m_joints.end(); }
private:
	// Iterator to first joint
	auto begin() { return m_joints.begin(); }

	// Iterator to last joint
	auto end() { return m_joints.end(); }

	auto headIt()       const { return begin(); }
	auto afterHeadIt()  const { return begin()+1; }
	auto tailIt()       const { return end()-1; }
	auto beforeTailIt() const { return end()-2; }

	auto headIt()       { return begin(); }
	auto afterHeadIt()  { return begin()+1; }
	auto tailIt()       { return end()-1; }
	auto beforeTailIt() { return end()-2; }
public:
	auto head()       const { return *headIt(); }
	auto afterHead()  const { return *afterHeadIt(); }
	auto tail()       const { return *tailIt(); }
	auto beforeTail() const { return *beforeTailIt(); }

	auto head() { return *headIt(); }

	auto afterHead() { return *afterHeadIt(); }

	auto tail() { return *tailIt(); }

	auto beforeTail() { return *beforeTailIt(); }

	// Moves snake's head and tail. Invalidates all iterators and possibly
	// modifies joints();
	void advance(Direction direction) {
		advanceTail();
		advanceHead(direction);
	}

	bool tryEat(Direction direction, const auto& food) {
		auto newHead{ head() };
		move(newHead, direction);

		if (newHead != food) return false;

		advanceHead(direction);
		return true;
	}

	auto joints() const { return m_joints.size(); }
	auto joints() { return m_joints.size(); }

	bool collidesWith(Point<auto> point) const {
		for (auto it{ begin()+1 }; it != end(); ++it)
			if (point.isBetween(it[-1], *it)) return true;

		return false;
	}
private:
	// The snake's joints plus the head and the tail
	std::deque<T> m_joints{ { 10, 1 }, { 1, 1 } };

	auto headDirection() const {
		return lineDirection(head(), afterHead());
	}
	auto tailDirection() const {
		return lineDirection(beforeTail(), tail());
	}

	void advanceHead(Direction direction) {
		if (direction != headDirection())
			m_joints.push_front(head());

		move(*headIt(), direction);
	}

	void advanceTail() {
		move(*tailIt(), tailDirection());

		if (tail() == beforeTail())
			m_joints.pop_back();
	}
};

#endif // !SNAKE_HPP
