#ifndef ATTR_HPP
#define ATTR_HPP

#include "Color.hpp"
#include "Window.hpp"

// Curses attribute
class Attr {
public:
	enum Enum{ bold };
	Attr(Color f, Color b = Color::ddefault);
	Attr(Enum);
private:
	char_t m_attr{};
	static short nextColorPair;

	friend auto operator|(Attr, auto);
	friend auto operator|(auto, Attr);
};

auto operator|(Attr a, auto o) { return a.m_attr | o; }
auto operator|(auto o, Attr a) { return a.m_attr | o; }

#endif // !ATTR_HPP
